#include "Arragementer.h"

extern Steder *steder;

Arrangementer::Arrangementer () {
	arrangementer = new List (Sorted);							// Lager sortert liste
	sisteArrangement = 0;										// init	

	ifstream inn ("ARRANGEMENTER.DTA");
	
	if (inn) {
		inn >> sisteArrangement;								// Leser inn antall arrangementer
		inn.ignore ();											// Lager gitt antall arrangementer
		for (int i = 1; i <= sisteArrangement; i++) {
			inn.ignore ();
			char *t = new char[STRLEN];
			inn.getline (t, STRLEN);							// Leser navn
			Arrangement *arr = new Arrangement (t, inn);		// Laster inn arrangement
			arrangementer->add (arr);							// Legger det til listen
		}

		for (int i = 1; i <= arrangementer->no_of_elements (); i++) {
			Arrangement *arr = (Arrangement*)arrangementer->remove_no (i);
			if (arr->nummer > sisteArrangement)
				sisteArrangement = arr->nummer;					// Setter siste arrangement til � v�re lik det h�yeste arr-nummeret
			arrangementer->add (arr);
			}
	} else
		cout << "\nKunne ikke finne \"ARRANGEMENTER.DTA\"";

	inn.close();												// Lukker fil
}

void Arrangementer::ny () {
	if (steder->steder->no_of_elements () > 0) {
		char *s = new char[STRLEN], *t = new char[STRLEN];
	
		do {
			les ("Sted", s, STRLEN);								// Leser stednavn
		} while (!steder->steder->in_list (s));

		les ("Arrangement navn", t, STRLEN);						// Leser arrangement navn
		Arrangement *arr = new Arrangement (t, s, ++sisteArrangement); // Lager nytt arrangement
		arrangementer->add (arr);									// Legger til listen
	} else 
		cout << "\nIngen steder eksisterer!";
}

void Arrangementer::lagre () {
	ofstream ut ("ARRANGEMENTER.DTA");

	ut << arrangementer->no_of_elements () << '\n';				// Skriver antall arrangementer
																// G�r gjennom alle arrangementer
	for (int i = 1; i <= arrangementer->no_of_elements (); i++) {
		Arrangement *arr = (Arrangement*)arrangementer->remove_no (i);
		arr->lagre (ut);										// Skriver ut arrangement
		arrangementer->add (arr);
	}

	ut.close ();
}

void Arrangementer::kjop () {
	if (arrangementer->no_of_elements () > 0) {
		int n;
	
		do {
			n = les ("Arrangement nummer", 1, sisteArrangement);	// Finner arrangement
		} while (!nummerTest (n));
																	// G�r gjennom alle arrangementer
		for (int i = 1; i <= arrangementer->no_of_elements (); i++) {
			Arrangement *arr = (Arrangement*)arrangementer->remove_no (i);
			if (arr->nummerTest (n))								// Tester om riktig nummer
				arr->kjop ();										// Kj�per
			arrangementer->add (arr);
		}
	} else
		cout << "\nIngen arrangementer eksisterer!";
}

bool Arrangementer::nummerTest (int n) {
																// G�r gjennom alle arrangementer
	for (int i = 1; i <= arrangementer->no_of_elements (); i++) {
		Arrangement *arr = (Arrangement*)arrangementer->remove_no (i);
		if (arr->nummerTest (n)) {
			arrangementer->add (arr);							// Return true hvis riktig nummer
			return true;
		}
		arrangementer->add (arr);
	}
	return false;
}

void Arrangementer::slett () {
	if (arrangementer->no_of_elements () > 0) {
		int n;

		do {
			n = les ("Arrangement nummer", 1, sisteArrangement);	// Leser arrangement nummer
		} while (!nummerTest (n));

		for (int i = 1; i <= arrangementer->no_of_elements (); i++) {
			Arrangement *arr = (Arrangement*)arrangementer->remove_no (i);
			if (arr->nummerTest (n))
				arr->slett ();										// Sletter hvis riktig nummer
			else 
				arrangementer->add (arr);
		}
	} else
		cout << "\nIngen arrangementer eksisterer!";
}

void Arrangementer::display () {
	char kommando;
	
	cout << "\n\nF�LGENDE KOMMANDOER ER TILGJENGELIG:\n";
	cout << "\t1   --> Vis data for alle arrangementer\n";
	cout << "\t2   --> Vis data for arrangementer som inneholder tekst\n";
	cout << "\t3   --> Vis data for arrangementer som holdes p� et vist sted\n";
	cout << "\t4   --> Vis data for arrangementer som holdes p� gitt dato\n";
	cout << "\t5   --> Vis data for arrangementer av en viss type\n";
	cout << "\t6   --> Vis data for arrangementer som holdes av artist\n";
	cout << "\t7   --> Vis utvidet data for arrangement med gitt nr.\n";
	cout << "\tAnnen input sender deg tilbake til hovedmenyen!\n";

	kommando = les("Kommando");
	switch (kommando) {
		case '1': {
			arrangementer->display_list ();											// Skriver alle arrangementer
			}
		break;

		case '2': {
			char *tekst = new char[STRLEN];

			les ("S�ketekst", tekst, STRLEN);										// Bruker-input
																					// G�r gjennom alle arrangementer
			for (int i = 1; i <= arrangementer->no_of_elements (); i++) {
				Arrangement *arr = (Arrangement*)arrangementer->remove_no (i);		// Henter ut fra listen
				if (arr->inneholderTekst (tekst))								
					arr->display ();												// Skriver hvis navnet inneholder s�ketekst
				arrangementer->add (arr);											// Legger tilbake i listen
			}}
		break;

		case '3': {
			char *s = new char[STRLEN];

			les ("Sted", s, STRLEN);												// Bruker-input
																					// G�r gjennom alle arrangementer
			for (int i = 1; i <= arrangementer->no_of_elements (); i++) {
				Arrangement *arr = (Arrangement*)arrangementer->remove_no (i);		// Henter ut fra listen
				if (arr->stedTest (s))
					arr->display ();												// Skriver hvis sted er likt bruker-input
				arrangementer->add (arr);											// Legger tilbake i listen
			}}
		break;

		case '4': {
			int d;

			d = les ("Dato", 20150101, 20301231);									// Bruker-input
																					// G�r gjennom alle arrangementer
			for (int i = 1; i <= arrangementer->no_of_elements (); i++) {
				Arrangement *arr = (Arrangement*)arrangementer->remove_no (i);		// Henter ut fra listen
				if (arr->datoTest (d))
					arr->display ();												// Skriver hvis dato er lik bruker-input
				arrangementer->add (arr);											// Legger tilbake i listen
			}}
		break;

		case '5': {
			int t;

			cout << "\n(Musikk=0, Sport=1, Teater=2, Show=3, Kino=4, Familie=5, Festival=6)";
			t = les ("Type", 0, 6);													// Bruker-input (int)
																					// G�r gjennom alle arrangementer
			for (int i = 1; i <= arrangementer->no_of_elements (); i++) {
				Arrangement *arr = (Arrangement*)arrangementer->remove_no (i);		// Henter ut fra listen
				if (arr->typeTest (t))
					arr->display ();												// Skriver hvis type er lik bruker-input
				arrangementer->add (arr);											// Legger tilbake i listen
			}}
		break;

		case '6': {
			char *a = new char[STRLEN];

			les ("Artist", a, STRLEN);												// Bruker-input
																					// G�r gjennom alle arrangementer
			for (int i = 1; i <= arrangementer->no_of_elements (); i++) {
				Arrangement *arr = (Arrangement*)arrangementer->remove_no (i);		// Henter ut fra listen
				if (arr->artistTest (a))
					arr->display ();												// Skriver hvis artist er lik bruker-input
				arrangementer->add (arr);											// Legger tilbake i listen
			}}
		break;

		case '7': {
			if (arrangementer->no_of_elements () > 0) {
				int n;

				n = les ("Arrangement nr.", 1, sisteArrangement);						// Bruker-input

				for (int i = 1; i <= arrangementer->no_of_elements (); i++) {
					Arrangement *arr = (Arrangement*)arrangementer->remove_no (i);		// Henter ut fra listen
					if (arr->nummerTest (n))
						arr->displayAll ();												// Skriver hvis artist er lik bruker-input
					arrangementer->add (arr);											// Legger tilbake i listen
				}
			} else
				cout << "\nIngen arrangementer eksisterer!";
		}
		break;
	}
}