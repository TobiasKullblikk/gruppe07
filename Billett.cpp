#include "Billett.h"

Billett::Billett (int n) : Num_element (n) { }

void Billett::lagreStol (ofstream &ut) {
	ut << number << ' ' << kundeNr << '\n';						// Skriver plassnummer og kundenummer til fil
}

void Billett::lagreVrimle (ofstream &ut) {
	ut << "1 " << kundeNr << '\n';								// Skriver plassnummer 1 og kundenummer til fil for vrimle
}

void Billett::displayStol (const int lengde) {
	cout << "\nRad " << (number / lengde);						// Regner ut rad
	cout << " sete " << (number % lengde);						// Regner ut Sete
	cout << " reservert av kunde nr " << kundeNr;				// Skriver kundenummer
}

void Billett::displayVrimle () {
	cout << "\nPlass reservert av kunde nr " << kundeNr;		// Skriver hvis kunde har kj�pt vrimleplass
}