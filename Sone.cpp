#include "Sone.h"

Sone::Sone (char t[]) : Text_element (t) {
	antSolgt = 0;										// Init
	pris = les ("Pris", 0, MAX_PRIS);					// Leser data fra bruker
}

Sone::Sone (char t[], ifstream &inn) : Text_element (t) {
	inn >> antTilSalgs; inn.ignore ();					// Laster inn data fra fil
	inn >> pris; inn.ignore ();
	antSolgt = 0;										// Init
	billetter = new List (Sorted);					
}

void Sone::kjop () { }

void Sone::lagreBilletter (ofstream &ut) { }

void Sone::lagre (ofstream &ut) { }

void Sone::lesBilletter (ifstream &inn) {
	inn >> antSolgt; inn.ignore ();						// Laster inn billetter

	for (int i = 1; i <= antSolgt; i++) {
		int n;
		inn >> n; inn.ignore ();						// Leser sete nummer
		Billett *billett = new Billett (n);
		inn >> billett->kundeNr; inn.ignore ();			// Leser kundenummer
		billetter->add (billett);						// Legger billett til listen
	}
}

void Sone::display () {
	cout << '\n' << text;								// Skriver sone-data
	cout << "\nPris: " << pris;

}