#include "listtool2.h"
#include <iostream>
#include <fstream>

using namespace std;

class Billett : public Num_element {
	public:
		int kundeNr;							// Trenger access
		Billett (int n);						// Konstruktor med n som sete nummer
		void lagreStol (ofstream &ut);			// Lagrer billett for stol
		void lagreVrimle (ofstream &ut);		// Lagrer billett for vrimle
		void displayStol (int lengde);			// Viser stol plass
		void displayVrimle ();					// Viser vrimle plass
};