﻿/*
	Dette er et program som holder orden på ulike kunders kjøp av billetter til ulike arrangementer 
	på ulike spille-/arrangementsteder. Ett	og samme spillested kan ha flere (stol)oppsett, 
	hvert bestående av en eller flere soner.

	Programmet inneholder følgende funksjoner
		- Legge inn / endre / skrive en kunde
		- Legge inn / skrive et spille-/arrangementsted
		- Legge inn / endre / skrive (stol)oppsett på et spille-/arrangementsted
		- Legge inn / skrive / slette / kjøpe billett(er) på et arrangement
		- Lese fra / skrive til filer hele/deler av datastrukturen 

	Løsningen er laget av Tobias Kullblikk, Aleksander Qvist Jacobsen & Andreas Ringstad.
*/

#include "Source.h"

Kunder *kunder;
Arrangementer *arrangementer;
Steder *steder;

int main () {
	char kommando;

	kunder = new Kunder ();								// Initsialiserer
	steder = new Steder ();
	arrangementer = new Arrangementer ();

	skrivMeny ();
	kommando = les ("Kommando");						// Bruker-input
	while (kommando != 'Q')  {
		switch (kommando)  {
			case 'K': kundeValg ();			break;
			case 'S': stedValg ();			break;
			case 'A': arrangementValg ();	break;
			case 'O': oppsettValg ();		break;
			default:  /*skrivMeny ();*/		break;
		}
		skrivMeny ();
		kommando = les ("Kommando");					// Looper til brukeren gir kommando Q
	}

	lagre ();
	return 0;
}

void skrivMeny ()  {
  cout << "\n\nFØLGENDE KOMMANDOER ER TILGJENGELIG:\n";
  cout << "\tA   --> Arrangement-valg\n";
  cout << "\tK   --> Kunde-valg\n";
  cout << "\tS   --> Sted-valg\n";
  cout << "\tO   --> Oppsett-valg\n";
  cout << "\tQ   --> Lagre og avslutt\n";
}

void kundeValg () {
	char kommando;

	cout << "\n\nFØLGENDE KOMMANDOER ER TILGJENGELIG:\n";
	cout << "\tD   --> Vis kunde-data\n";
	cout << "\tN   --> Ny kunde\n";
	cout << "\tE   --> Endre kunde-data\n";
	cout << "\tAnnen input sender deg tilbake til hovedmenyen!\n";

	kommando = les ("Kommando");				// Lar bruker velge, ikke gyldig kommando resulterer i navigerering tilbake til hovedmeny
	switch (kommando) {
		case 'D': kunder->display ();	break;
		case 'N': kunder->ny ();		break;
		case 'E': kunder->endre ();		break;
	}
}

void stedValg () {
	char kommando;

	cout << "\n\nFØLGENDE KOMMANDOER ER TILGJENGELIG:\n";
	cout << "\tD   --> Vis sted-data\n";
	cout << "\tN   --> Nytt sted\n";
	cout << "\tAnnen input sender deg tilbake til hovedmenyen!\n";

	kommando = les ("Kommando");				// Lar bruker velge, ikke gyldig kommando resulterer i navigerering tilbake til hovedmeny
	switch (kommando) {
		case 'D': steder->display ();	break;
		case 'N': steder->ny ();		break;
	}
}

void arrangementValg () {
	char kommando;

	cout << "\n\nFØLGENDE KOMMANDOER ER TILGJENGELIG:\n";
	cout << "\tD   --> Vis arrangement-data\n";
	cout << "\tN   --> Nytt arrangement\n";
	cout << "\tK   --> Kjøp billett(er)\n";
	cout << "\tS   --> Slett arrangement\n";
	cout << "\tAnnen input sender deg tilbake til hovedmenyen!\n";

	kommando = les ("Kommando");				// Lar bruker velge, ikke gyldig kommando resulterer i navigerering tilbake til hovedmeny
	switch (kommando) {
		case 'D': arrangementer->display ();	break;
		case 'N': arrangementer->ny ();			break;
		case 'K': arrangementer->kjop ();		break;
		case 'S': arrangementer->slett ();		break;
	}
}

void oppsettValg () {
	char kommando;

	cout << "\n\nFØLGENDE KOMMANDOER ER TILGJENGELIG:\n";
	cout << "\tD   --> Vis oppsett-data\n";
	cout << "\tN   --> Nytt oppsett\n";
	cout << "\tE   --> Endre oppsett\n";
	cout << "\tAnnen input sender deg tilbake til hovedmenyen!\n";

	kommando = les ("Kommando");				// Lar bruker velge, ikke gyldig kommando resulterer i navigerering tilbake til hovedmeny
	switch (kommando) {
		case 'D': steder->displayOppsett ();	break;
		case 'N': steder->nyOppsett ();			break;
		case 'E': steder->endreOppsett ();		break;
	}
}

void lagre () {
	kunder->lagre ();							// Lagrer data
	steder->lagre ();
	arrangementer->lagre ();
}