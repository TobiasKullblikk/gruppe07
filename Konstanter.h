#ifndef __KONSTANTER_H_INCLUDED__
#define __KONSTANTER_H_INCLUDED__		// Definerer Konstanter.h slik at den ikke gjen-inkluderes

const int NVNLEN = 20;				// Maks lengde for navn
const int STRLEN = 50;				// Maks string lengde
const int MAX_OPPSETT = 5;			// Maks antall oppsett
const int MAX_PRIS = 9999;			// Maks pris
const int MAX_LENGDE = 150;			// for stol-soner
const int MAX_RADER = 150;
const int MAX_PLASSER = 22500;		// Maks antall plasser

#endif