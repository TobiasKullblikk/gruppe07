#include "Stoler.h"

extern Kunder *kunder;

Stoler::Stoler (char t[]) : Sone (t) {
	rader = les ("Rader", 1, MAX_RADER);
	lengde = les ("Rad lengde", 1, MAX_LENGDE);
	antTilSalgs = rader * lengde;
}

Stoler::Stoler (char t[], ifstream &inn) : Sone (t, inn) {
	inn >> rader; inn.ignore ();
	inn >> lengde; inn.ignore ();
}

void Stoler::kjop () {
	int rad, sete, ant, kundeNr;

	do {
		rad = les ("Rad", 1, rader);
		sete = les ("Sete", 1, lengde);
		ant = les ("Antall plasser (Mot h�yre fra vlagt plass)", 1, lengde);

	} while (!plassLedigTest (rad, sete, ant));

	do {
		kundeNr = les ("Kundenummer", 1, kunder->sisteKunde);
	} while (!kunder->kunder->in_list (kundeNr));
	
	ofstream ut("BILLETTER.DTA", std::ios_base::app);

	for (int i = 0; i < ant; i++) {
		antSolgt++;
		Billett *billett = new Billett ((rad * lengde) + sete + i);
		billett->kundeNr = kundeNr;
		billetter->add (billett);

		Kunde *kunde = (Kunde*)kunder->kunder->remove (kundeNr);
		ut << kunde->navn << " (" << kundeNr << ") har kj�pt plass p� rad " << rad << " sete " << sete + i << ".\n";
		kunder->kunder->add (kunde);
	}

	ut.close ();
}

bool Stoler::plassLedigTest (int rad, int sete, int ant) {
	for (int i = 0; i < ant; i++)
		if (billetter->in_list ((rad * lengde) + sete + i) || (sete + i > lengde))
			return false;
	
	return true;
}

void Stoler::lagreBilletter (ofstream &ut) {
	ut << '\n' << text << '\n';
	ut << "1\n";
	ut << antTilSalgs << '\n';
	ut << pris << '\n';
	ut << rader << '\n';
	ut << lengde << '\n';
	ut << antSolgt << '\n';

	for (int i = 1; i <= billetter->no_of_elements (); i++) {
		Billett *billett = (Billett*)billetter->remove_no (i);
		billett->lagreStol (ut);
		billetter->add (billett);
	}
}

void Stoler::lagre (ofstream &ut) {
	ut << text << '\n';
	ut << "1\n";
	ut << antTilSalgs << '\n';
	ut << pris << '\n';
	ut << rader << '\n';
	ut << lengde << '\n';
}

void Stoler::display () {
	Sone::display ();
	cout << "\nStolplasser";
	cout << "\nAntall rader: " << rader;
	cout << "\nRad lengde: " << lengde;
	cout << "\nAntall billetter til salgs: " << antTilSalgs;
	if (antSolgt > 0) {
			cout << "\nAntall billetter solgt: " << antSolgt;
																			// G�r gjennom alle bilettene i sonen
		for (int i = 1; i <= billetter->no_of_elements (); i++) {
			Billett *billett = (Billett*)billetter->remove_no (i);			// Henter ut fra listen
			billett->displayStol (lengde);									// Skriver kundenummer og plass
			billetter->add (billett);										// Legger tilbake til listen
	}}
}