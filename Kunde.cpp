#include "Kunde.h"

Kunde::Kunde (int n) : Num_element (n) {
	navn = new char[NVNLEN];
	adresse = new char[STRLEN];
	poststed = new char[STRLEN];
	epost = new char[STRLEN];

	les ("Navn", navn, NVNLEN);												// Leser data fra bruker
	telefon = les ("Telefon", 1000000, 99999999);
	les ("Adresse", adresse, STRLEN);
	postnummer = les ("Postnummer", 1, 9991);
	les ("Poststed", poststed, STRLEN);
	les ("Epost", epost, STRLEN);
}

Kunde::Kunde (int n, ifstream &inn) : Num_element (n) {
	navn = new char[NVNLEN];
	adresse = new char[STRLEN];
	poststed = new char[STRLEN];
	epost = new char[STRLEN];

	inn.getline (navn, NVNLEN);												// Leser data fra fil
	inn >> telefon; inn.ignore ();
	inn.getline (adresse, STRLEN);
	inn >> postnummer; inn.ignore ();
	inn.getline (poststed, STRLEN);
	inn.getline (epost, STRLEN);
}

void Kunde::lagre (ofstream &ut) {
	ut << '\n' << number << '\n';											// Skriver data til fil
	ut << navn << '\n';
	ut << telefon << '\n';
	ut << adresse << '\n';
	ut << postnummer << '\n';
	ut << poststed << '\n';
	ut << epost << '\n';
}

void Kunde::display () {
	cout << "\nKunde: " << number;											// Skriver ut all kunde-data
	cout << "\n\n" << navn << '\n' << telefon;
	cout << '\n' << adresse << '\n' << postnummer << " " << poststed;
	cout << '\n' << epost << '\n';
}

bool Kunde::inneholderNavn (const char t[]) { return (strstr (navn, t) != NULL); }

void Kunde::endre () {
	char kommando;

	cout << "\n\nF�LGENDE KOMMANDOER ER TILGJENGELIG:\n";
	cout << "\t1   --> Endre navn\n";
	cout << "\t2   --> Endre telefon\n";
	cout << "\t3   --> Endre adresse\n";
	cout << "\t4   --> Endre postnummer\n";
	cout << "\t5   --> Endre poststed\n";
	cout << "\t6   --> Endre epost\n";
	cout << "\tM   --> Sender deg tilbake til hovedmenyen!\n";

	kommando = les ("Kommando");
																			// Lar brukeren velge hva som skal endres p�
	while (kommando != 'M')  {
		switch (kommando) {
			case '1': les ("Navn", navn, NVNLEN);					break;
			case '2': telefon = les ("Telefon", 1000000, 99999999); break;
			case '3': les ("Adresse", adresse, STRLEN);				break;
			case '4': postnummer = les ("Postnummer", 1, 9991);		break;
			case '5': les ("Poststed", poststed, STRLEN);			break;
			case '6': les ("Epost", epost, STRLEN);					break;
		}
	kommando = les ("Kommando");											// Looper til brukeren gir kommando M
	}
}