#include "Sone.h"

using namespace std;

class Vrimle : public Sone {
	public:
		Vrimle (char t[]);								// Konstruktor med tekst
		Vrimle (char t[], ifstream &inn);				// Konstruktor med tekst og ifstream for innlasting
		virtual void kjop ();							// Kjope billett i sonen
		void lagreBilletter (ofstream &ut) override;	// Lagrer billetter
		void lagre (ofstream &ut) override;				// Lagrer sonen
		void display ();								// Viser sonen
};