#include "listtool2.h"
#include "Felles.h"
#include "Konstanter.h"
#include <iostream>				// cout

using namespace std;

class Kunde : public Num_element {
	private:
		int postnummer, telefon;
		char *adresse, *poststed, *epost;
	public:
		Kunde (int n);							// Konstruktor med kundenummer
		Kunde (int n, ifstream &inn);			// Konstruktor med kundenummer og ifstream for innlasting
		bool inneholderNavn (const char t[]);	// Tester om parameter finnes i navnet til kunden
		void endre ();							// Valg for endring av kunde-data
		void lagre (ofstream &ut);				// Lagre til kunder fil
		void display ();						// Skriver ut kunde-data
		char *navn;								// Trenger access
};
