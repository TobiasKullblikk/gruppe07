#include "Vrimle.h"

extern Kunder *kunder;

Vrimle::Vrimle (char t[]) : Sone (t) {
	antTilSalgs = les ("Antall plasser", 1, MAX_PLASSER);			// Leser antall plasser i vrimle sone fra bruker
}

Vrimle::Vrimle (char t[], ifstream &inn) : Sone (t, inn) { }

void Vrimle::kjop () {
	int ant, kundeNr;

	ant = les ("Antall plasser", 1, antTilSalgs - antSolgt);		// Leser antall billetter

	do {
		kundeNr = les ("Kundenummer", 1, kunder->sisteKunde);		// Leser kundenummer
	} while (!kunder->kunder->in_list (kundeNr));
	
	ofstream ut("BILLETTER.DTA", std::ios_base::app);				// Skriver ut billetter p� fil
																	// G�r gjennom for hver billett som �nskes � kj�pes
	for (int i = 0; i < ant; i++) {
		antSolgt++;													// �ker antall solgt
		Billett *billett = new Billett (1);							// Lager billett
		billett->kundeNr = kundeNr;									// Setter gitt kundenummer
		billetter->add (billett);									// Legger billetten til listen

		Kunde *kunde = (Kunde*)kunder->kunder->remove (kundeNr);
		ut << kunde->navn << " (" << kundeNr << ") har kj�pt en plass.\n"; // Skriver billett
		kunder->kunder->add (kunde);
	}
	ut.close ();
		
	
}

void Vrimle::lagreBilletter (ofstream &ut) {
	ut << '\n' << text << '\n';										// Skriver ut sone-salg-data
	ut << "0\n";
	ut << antTilSalgs << '\n';
	ut << pris << '\n';
	ut << antSolgt << '\n';

	for (int i = 1; i <= billetter->no_of_elements (); i++) {
		Billett *billett = (Billett*)billetter->remove_no (i);
		billett->lagreVrimle (ut);									// Skriver ut vrimle-billett
		billetter->add (billett);
	}
}

void Vrimle::lagre (ofstream &ut) {
	ut << text << '\n';												// Skriver ut sonenavn, type, antTilSalgs og pris
	ut << "0\n";
	ut << antTilSalgs << '\n';
	ut << pris << '\n';
}

void Vrimle::display () {
	Sone::display ();
	cout << "\nSt�sone";
	cout << "\nAntall billetter til salgs: " << antTilSalgs;

	if (antSolgt > 0) {
			cout << "\nAntall billetter solgt: " << antSolgt;
																	// G�r gjennom alle bilettene i sonen
		for (int i = 1; i <= billetter->no_of_elements (); i++) {
			Billett *billett = (Billett*)billetter->remove_no (i);	// Henter ut fra listen
			billett->displayVrimle ();								// Skriver kundenummer
			billetter->add (billett);								// Legger tilbake til listen
	}}
}