#include "Kunder.h"

Kunder::Kunder () {
	int n = 0;
	sisteKunde = 0;												// inti
	kunder = new List (Sorted);									// Lager sortert liste

	ifstream inn ("KUNDER.DTA");
	
	if (inn) {
		inn >> sisteKunde;										// Leser antall kunder
		inn.ignore ();											// Lager gitt antall kunder
		while (sisteKunde > n) {
			inn.ignore (); inn >> n; inn.ignore ();
			Kunde *kunde = new Kunde (n, inn);					// Lager kunde fra innlest tekst
			kunder->add (kunde);								// Legger kunde til listen
			}
	} else
		cout << "\nKunne ikke finne \"KUNDER.DTA\"";

	inn.close();					// Lukker fil
}

void Kunder::display () {
	char kommando;

	cout << "\n\nF�LGENDE KOMMANDOER ER TILGJENGELIG:\n";
	cout << "\tA   --> Vis data for alle kunder\n";
	cout << "\tE   --> Vis data for en kunde med gitt kundenr\n";
	cout << "\tN   --> Skriver data for alle kunder med gitt navn\n";
	cout << "\tAnnen input sender deg tilbake til hovedmenyen!\n";

	kommando = les("Kommando");									// Bruker-valg
	switch (kommando) {
		case 'A': {
			kunder->display_list ();							// Skriver alle kunder
			}
		break;

		case 'E': {
			int n = les ("Nummer", 1, sisteKunde);				// Leser kundenummer fra bruker
			kunder->display_element (n);
			}
		break;

		case 'N': {
			char *t = new char[NVNLEN];
			les ("Navn / Delnavn", t, NVNLEN);					// Leser navn / delnavn fra bruker
																// G�r gjennom alle kunder
			for (int i = 1; i <= sisteKunde; i++) {
				Kunde *kunde = (Kunde*)kunder->remove_no (i);		// Henter ut kunde
				if (kunde->inneholderNavn (t))
					kunde->display ();							// Skriver kunde-data hvis kunden inneholder bruker-input
				kunder->add (kunde);								// Legger tilbake i listen
			}}
		break;
	}
}

void Kunder::ny () {
	Kunde *kunde = new Kunde (++sisteKunde);					// �ker sisteKunde med en og lager kunde objekt
	kunder->add (kunde);										// Legger til i listen med alle kunder
}

void Kunder::endre () {
	int n;

	n = les ("Kundenummer", 1, sisteKunde);						// Leser kundenummer fra bruker

	Kunde *kunde = (Kunde*)kunder->remove (n);					// Henter ut fra listen
	kunde->endre ();											// Gir brukeren endrings alternativer
	kunder->add (kunde);										// Legger tilbake i listen
}

void Kunder::lagre () {
	ofstream ut ("KUNDER.DTA");

	ut << sisteKunde << '\n';

	for (int i = 1; i <= kunder->no_of_elements (); i++) {
		Kunde *kunde = (Kunde*)kunder->remove_no (i);			// Henter ut kunde
		kunde->lagre (ut);										// Skriver kunde-data hvis kunden inneholder bruker-input
		kunder->add(kunde);										// Legger tilbake i listen
	}

	ut.close ();
}