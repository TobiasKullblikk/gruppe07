#ifndef __ARRANGEMENT_H_INCLUDED__
#define __ARRANGEMENT_H_INCLUDED__		// Definerer Arrangement.h slik at den ikke gjen-inkluderes

#include "listtool2.h"
#include "Steder.h"
#include "Enumer.h"
#include "Konstanter.h"
#include <cstdio>


class Arrangement : public Text_element {
	private:
		char *spillested;
		char *artist;
		int dato;
		int minutt;
		int time;
		ArrangementType type;						// Enum for type
		Oppsett *oppsett;							// Kopi av oppsettet
	public:
		Arrangement (char t[], ifstream &inn);		// Constructor
		Arrangement (char t[], char s[], int n);
		bool inneholderTekst (const char t[]);		// Ser om arrangement navn inneholder tekst
		bool stedTest (const char t[]);				// Ser om spillested er lik parameter
		bool datoTest (int d);						// Ser om dato er lik parameter
		bool typeTest (int t);						// Ser om arrangement type er lik parameter (int)
		bool artistTest (const char t[]);			// Ser om artist er lik parameter
		bool nummerTest (int n);					// Ser om nummer er like parameter
		void kjop ();								// Kjop billett 
		void slett ();								// Slett Arrangement
		void lagreOppsett ();						// Lagrer data om oppsett
		void lastOppsett ();						// Last inn data om oppsett
		void lagre (ofstream &ut);					// Lagre til fil
		void display ();							// Skriver ut informasjon om arrangementet
		void displayAll ();							// Skriver ut informasjon om arrangementet og solgte biletter
		int nummer;									// Trenger access
};

#endif