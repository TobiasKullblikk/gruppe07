#ifndef __SONE_H_INCLUDED__
#define __SONE_H_INCLUDED__						// Definerer Sone.h slik at den ikke gjen-inkluderes

#include "listtool2.h"
#include "Billett.h"
#include "Konstanter.h"
#include "Felles.h"
#include "Kunder.h"
#include <iostream>
#include <fstream>

using namespace std;

class Sone : public Text_element {
	protected: 
		int antTilSalgs, antSolgt, pris;
		List *billetter;							// Liste over biletter, number verdien i hvert element er plass nr.
	public:
		Sone (char t[]);							// Konstruktor med tekst
		Sone (char t[], ifstream &inn);				// Konstruktor med tekst og ifstream for innslasting
		virtual void kjop () = 0;					// For barn-kommandoer
		virtual void lagreBilletter (ofstream &ut);	// For barn-kommandoer
		virtual void lagre (ofstream &ut);			// For barn-kommandoer
		void lesBilletter (ifstream &inn);			// Leser billetter fra fil
		void display ();							// Skriver ut sone data
};

#endif