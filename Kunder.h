#ifndef __KUNDER_H_INCLUDED__
#define __KUNDER_H_INCLUDED__		// Definerer Kunder.h slik at den ikke gjen-inkluderes

#include "Kunde.h"

class Kunder {
	public:
		Kunder ();			// Konstruktor
		void display ();	// Valg for utskrift av kunde-data
		void ny ();			// Legger til nytt kunde objekt
		void endre ();		// Finner kunde som skal endres
		void lagre ();		// Skriver til fil
		List *kunder;		// Liste med kunder
		int sisteKunde;		// Trenger access
};

#endif