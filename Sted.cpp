#include "Sted.h"

Oppsett::Oppsett () {
	soner = new List (Sorted);									// Lager sortert liste
}
																// Sender tekst parameter videre til forelder constructor
Sted::Sted (char t[]) : Text_element (t) {
	antOppsett = 0;
}					

Sted::Sted (char t[], ifstream &inn) : Text_element (t) {
	inn >> antOppsett; inn.ignore ();							// Leser antall oppsett fra fil

	for (int i = 1; i <= antOppsett; i++) {
		oppsett[i] = Oppsett ();
		int antSoner;
		inn >> antSoner; inn.ignore ();							// Leser antall soner i oppsett
		for (int o = 1; o <= antSoner; o++) {
			char *t = new char[STRLEN];
			int type;

			inn.getline (t, STRLEN);							// Leser sonenavn
			inn >> type; inn.ignore ();							// Leser type
			if (type == 0) {
				Vrimle *vsone = new Vrimle (t, inn);			// Hvis det er vrimle-sone
				oppsett[i].soner->add (vsone);
			} else {
				Stoler *ssone = new Stoler (t, inn);			// Hvis det er stol-sone
				oppsett[i].soner->add (ssone);
			}
			
		}
	}
}

void Sted::nyOppsett () {
	char kommando = 'N';
	
	if (antOppsett < MAX_OPPSETT) {
		if (antOppsett > 0) {
			cout << "\n�nsker du � kopiere ett eksisterende oppsett? (y/N)";
			kommando = les ("Kommando");
		}

		if (kommando == 'Y') {
			int n = les ("Oppsett", 1, antOppsett);
			oppsett[++antOppsett] = oppsett[n];						// Kopierer oppsett
		} else {
			oppsett[++antOppsett] = Oppsett ();						// Lager nytt oppsett
		}

		endreOppsett (antOppsett);									// Endre data i oppsett
	} else
		cout << "\nFor mange oppsett!!";
}

void Sted::velgOppsett () {
	int n;
	if (antOppsett > 0) {
		n = les ("Oppsett", 1, antOppsett);							// Leser hvilket oppsett
		endreOppsett (n);											// Endre oppsett
	} else
		cout << "\nIngen oppsett!";
}

void Sted::endreOppsett (int n) {
	char kommando;

	skrivOppsettMeny ();
	kommando = les("Kommando");									// Bruker-valg
	while (kommando != 'M')  {
		switch (kommando) {
			case '1': {
				char *t = new char[STRLEN];
				do {
					les ("Sonenavn", t, STRLEN);				// Les sonenavn
				} while (oppsett[n].soner->in_list (t));
				
				Stoler *ssone = new Stoler (t);					// Ny stolsone
				oppsett[n].soner->add (ssone);
				}
			break;

			case '2': {
				char *t = new char[STRLEN];
				do {
					les ("Sonenavn", t, STRLEN);				// Les sonenavn
				} while (oppsett[n].soner->in_list (t));
				
				Vrimle *vsone = new Vrimle (t);					// Ny vrimlesone
				oppsett[n].soner->add (vsone);
				}
			break;

			case '3': {
				char *t = new char[STRLEN];

				if (oppsett[n].soner->no_of_elements () > 0) {
					do {
						les ("Sonenavn", t, STRLEN);			// Les sonenavn
					} while (!oppsett[n].soner->in_list (t));
				
					oppsett[n].soner->destroy (t);				// Slett sone
				} else
					cout << "\nIngen soner i oppsett!";
				}
			break;
		}
		skrivOppsettMeny ();
		kommando = les("Kommando");
	}
}

void Sted::skrivOppsettMeny () {
	cout << "\n\nF�LGENDE KOMMANDOER ER TILGJENGELIG:\n";
	cout << "\t1   --> Ny stolsone\n";
	cout << "\t2   --> Ny vrimlesone\n";
	cout << "\t3   --> Slett sone\n";
	cout << "\tM   --> Tilbake til hovedmenyen\n";
}

void Sted::lagre (ofstream &ut) {
	ut << '\n' << text << '\n';
	ut << antOppsett << '\n';												// Skriver antall oppsett
																			// G�r gjennom alle oppsett
	for (int o = 1; o <= antOppsett; o++) {
		ut << oppsett[o].soner->no_of_elements () << '\n';					// Skriver antall soner i oppsett og g�r gjennom
		for (int i = 1; i <= oppsett[o].soner->no_of_elements (); i++) {
			Sone *sone = (Sone*)oppsett[o].soner->remove_no (i);
			sone->lagre (ut);												// Lagrer sone
			oppsett[o].soner->add (sone);
		}
	}
}

void Sted::display () {
	cout << "\nSted: " << text;									// Skriver ut stednavn
	cout << "\nAntall stoloppsett: " << antOppsett << '\n';		// Skriver ut antall oppsett
}

void Sted::displayOppsett () {
	int n;

	if (antOppsett > 0) {
		n = les ("Oppsett", 1, antOppsett);
		oppsett[n].soner->display_list ();						// Viser gitt oppsett
	}
}