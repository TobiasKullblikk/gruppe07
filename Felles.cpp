#include "Felles.h"

using namespace std;

void les (const char t[], char s[], const int LEN) {  
  do  {
    cout << '\n' << t << ": ";	cin.getline(s, LEN);					// Ledetekst og leser
  } while (strlen (s) == 0);											// Sjekker at tekstlengden er ulik 0
}

int les (const char t[], const int min, const int max)  { 
	int n;

	do {
		cout << '\n' << t << " (" << min << '-' << max << "): ";
		while(true){
			cin >> n;													// Leser inn fra bruker
			if (cin.fail()){
				cin.clear();											// Fjerner ugyldig input
				cin.ignore();
			} else
				break;
		}
	} while(n < min  ||  n > max);										// Kj�rer til riktige verdier

  return n;																// Returnerer innlest tall
}

char les (const char t[])  {											// Henter ett ikke-blankt UPCASET tegn
  char ch;
  cout << '\n' << t << ":  ";											// Skriver medsendt ledetekst
  cin >> ch;   cin.ignore ();											// Leser ETT tegn. Forkaster '\n'
  return (toupper (ch));												// Upcaser og returnerer
}