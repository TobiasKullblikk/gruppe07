#include "Sted.h"

class Steder  {
	private:
		
	public:
		Steder ();				// Constructor
		void ny ();				// Lager nytt sted
		void nyOppsett ();		// Lager nytt oppsett
		void endreOppsett ();	// Endre p� oppsett
		void lagre ();			// Lagre sted
		void display ();		// Kj�rer display () for alle steder
		void displayOppsett ();	// Vis oppsett
		List *steder;			// Liste med alle steder
};