#include "Steder.h"

Steder::Steder () {
	int antall = 0;											// Init
	steder = new List (Sorted);								// Lager sortert liste

	ifstream inn ("STEDER.DTA");
	
	if (inn) {
		inn >> antall;										// Leser antall steder fra fil
		inn.ignore ();										// Lager gitt antall steder
		for (int i = 1; i <= antall; i++) {
			inn.ignore ();
			char *t = new char[STRLEN];
			inn.getline (t, STRLEN);						// Leser stednavn
			Sted *sted = new Sted (t, inn);					// Lager og laster inn sted
			steder->add (sted);								// Legger sted til listen
			}
	} else
		cout << "\nKunne ikke finne \"STEDER.DTA\"";

	inn.close();
}

void Steder::ny () {
	char *navn = new char[STRLEN];

	do {
		les ("Navn", navn, STRLEN);							// Leser navn fra bruker
	} while (steder->in_list (navn));						// Ser om sted med samme navn eksisterer

	Sted *sted = new Sted (navn);							// Lager sted
	steder->add (sted);										// Legger til listen
}

void Steder::nyOppsett () {
	char *navn = new char[STRLEN];

	do {
		les ("Sted", navn, STRLEN);							// Leser navn fra bruker
	} while (!steder->in_list (navn));						// Ser om sted med samme navn eksisterer

	Sted *sted = (Sted*)steder->remove (navn);
	sted->nyOppsett ();
	steder->add (sted);
}

void Steder::endreOppsett () {
	char *navn = new char[STRLEN];

	do {
		les ("Sted", navn, STRLEN);							// Leser navn fra bruker
	} while (!steder->in_list (navn));						// Ser om sted med samme navn eksisterer

	Sted *sted = (Sted*)steder->remove (navn);
	sted->velgOppsett ();
	steder->add (sted);
}

void Steder::lagre () {
	int antall;
	
	ofstream ut ("STEDER.DTA");

	ut << steder->no_of_elements () << '\n';				// Skriver antall steder

	for (int i = 1; i <= steder->no_of_elements (); i++) {
		Sted *sted = (Sted*)steder->remove_no (i);
		sted->lagre (ut);									// Skriver data for alle steder
		steder->add (sted);
	}

	ut.close ();
}

void Steder::display () {
	steder->display_list ();								// Kj�rer display () for alle steder
}

void Steder::displayOppsett () {
	char *t = new char[STRLEN];

	do {
		les ("Sted", t, STRLEN);							// Leser sted fra bruker
	} while (!steder->in_list (t));	
	
	Sted *sted = (Sted*)steder->remove (t);
	sted->displayOppsett ();								// Viser oppsett
	steder->add (sted);
}