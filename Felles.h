#ifndef __FELLES_H_INCLUDED__
#define __FELLES_H_INCLUDED__		// Definerer Felles.h slik at den ikke gjen-inkluderes

#include <fstream>					// ifstream, ofstream
#include <iostream>					// cin, cout
#include <cstring>					// strcmp, strstr
#include <cctype>					// toupper

using namespace std;

void les(const char t[], char s[], const int LEN);					// Les en ikke tom tekst
int les(const char t[], const int min, const int max);				// Les tall i interval
char les(const char t[]);													// Les ett ikke blankt UPCASE tegn

#endif