#include "Sone.h"

using namespace std;

class Stoler : public Sone {
	private:
		int rader, lengde;
	public:
		Stoler (char t[]);									// Konstruktor med tekst
		Stoler (char t[], ifstream &inn);					// Konstruktor med tekst og ifstream for innlasting
		virtual void kjop ();								// Kjop billett i sone
		bool plassLedigTest (int rad, int plass, int ant);	// Ser om det er ledige plasser
		void lagreBilletter (ofstream &ut) override;		// Lagrer billetter til fil
		void lagre (ofstream &ut) override;					// Lagrer sone til fil
		void display ();									// Viser sone
};