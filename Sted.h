#ifndef __STED_H_INCLUDED__
#define __STED_H_INCLUDED__					// Definerer Sted.h slik at den ikke gjen-inkluderes

#include "Felles.h"
#include "Vrimle.h"
#include "Stoler.h"

using namespace std;
											// Struktur for oppsett
struct Oppsett {
	List *soner;							// Liste med soner i oppsettet
	Oppsett ();								// Constructor for struct
};

class Sted : public Text_element {
	private:
		void skrivOppsettMeny ();			// Skriver ut oppsett menyen
		void endreOppsett (int n);			// Endre p� oppsetts
	public:
		Sted (char t[]);					// Lager sted med navn i parameter
		Sted (char t[], ifstream &inn);		// Lager sted fra fil
		void nyOppsett ();					// Lager nytt oppsett
		void velgOppsett ();				// Velge oppsett
		void lagre (ofstream &ut);			// Lagre til fil
		void display ();					// Skriver ut sted-data
		void displayOppsett ();				// Viser oppsett
		int antOppsett;						// Antall oppsett ved sted
		Oppsett oppsett[MAX_OPPSETT+1];		// Array med oppsett, bruker ikke 0
};

#endif