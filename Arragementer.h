#ifndef __ARRANGEMENTER_H_INCLUDED__
#define __ARRANGEMENTER_H_INCLUDED__		// Definerer Arrangementer.h slik at den ikke gjen-inkluderes

#include "Arragement.h"

using namespace std;

class Arrangementer {
	private:
		int sisteArrangement;
		List* arrangementer;		// Liste med Arrangement objekter
	public:
		Arrangementer ();			// Constructor
		void ny ();					// Nytt arrangement
		void lagre ();				// Lagre arrangementer
		void kjop ();				// Kj�p billett
		void slett ();				// Slett arrangement
		bool nummerTest (int n);	// Test arrangement nummer
		void display ();			// Gir brukeren valg om utskrift av arrangement data
};

#endif