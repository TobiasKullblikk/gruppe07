#include "Arragement.h"

extern Steder *steder;																// Extern til Steder

Arrangement::Arrangement (char t[], ifstream &inn) : Text_element (t) {
	int intType, antSoner;
	spillested = new char[STRLEN];
	artist = new char[STRLEN];

	inn >> nummer; inn.ignore ();													// Leser inn data for arrangement fra fil
	inn.getline (spillested, STRLEN);
	inn.getline (artist, STRLEN);
	inn >> dato; inn.ignore ();
	inn >> minutt; inn.ignore ();
	inn >> time; inn.ignore ();
	inn >> intType; type = static_cast<ArrangementType>(intType); inn.ignore ();	// Caster til ArrangementType
}

Arrangement::Arrangement (char t[], char s[], int n) : Text_element (t) {
	int o, intType;
	Sted *sted = (Sted*)steder->steder->remove (s);									// Henter sted
	artist = new char[STRLEN];
	spillested = s;
	nummer = n;

	o = les ("Oppsett", 1, sted->antOppsett);
	oppsett = &sted->oppsett[o];													// Kopierer oppsett
	steder->steder->add (sted);														// Legger tilbake sted

	les ("Artist", artist, STRLEN);													// Leser inn data fra bruker
	dato = les ("Dato", 20150101, 20301231);
	minutt = les ("Minutt", 0, 59);
	time = les ("Time", 0, 23);
	intType = les ("Type ( Musikk=0, Sport=1, Teater=2, Show=3, Kino=4, Familie=5, Festival=6 )", 0, 6);
	type = static_cast<ArrangementType>(intType);

	lagreOppsett ();																// Lagrer oppsett
	oppsett = NULL;
}

bool Arrangement::inneholderTekst (const char t[]) { return (strstr (text, t) != NULL); }

bool Arrangement::stedTest (const char t[]) { return (strcmp (t, spillested) == 0); }

bool Arrangement::datoTest (int d) { return (d == dato); }

bool Arrangement::typeTest (int t) { return (t == type); }

bool Arrangement::artistTest (const char t[]) { return (strcmp (t, artist) == 0); }

bool Arrangement::nummerTest (int n) { return (n == nummer); }

void Arrangement::kjop () {
	char *t = new char[STRLEN];

	lastOppsett ();																	// Laster inn oppsett

	do {
		les ("Sone", t, STRLEN);													// Leser sone
	} while (!oppsett->soner->in_list (t));											// Fortsetter til den finner sonen

	Sone *sone = (Sone*)oppsett->soner->remove (t);									// Henter ut sone
	sone->kjop ();																	// Kjoper billett
	oppsett->soner->add (sone);														// Legger tilbake billett

	lagreOppsett ();																// Lagrer oppsett

	oppsett = NULL;																	// Frigj�r oppsett fra minne
}

void Arrangement::slett () {
	char filename[32] = "ARR_", extension[5] = ".DTA";
	char number[5] = "0000";

	sprintf(number, "%04d", nummer);												// Setter sammen filnavn: "ARR_0001.DTA"
	strcat (filename, number);
	strcat (filename, extension);
	
	remove (filename);																// Sletter fil
}

void Arrangement::lagreOppsett () {
	char filename[32] = "ARR_", extension[5] = ".DTA";
	char number[5] = "0000";

	sprintf(number, "%04d", nummer);												// Setter sammen filnavn: "ARR_0001.DTA"
	strcat (filename, number);
	strcat (filename, extension);

	ofstream ut (filename);															

	ut << oppsett->soner->no_of_elements () << '\n';								// Skriver ut sone-antall
	
	for (int i = 1; i <= oppsett->soner->no_of_elements (); i++) {
		Sone *sone = (Sone*)oppsett->soner->remove_no (i);							// Henter ut sone
		sone->lagreBilletter (ut);													// Lagrer billetter i sone
		oppsett->soner->add (sone);													// Legger tilbake i listen
	}

	ut.close ();
}

void Arrangement::lastOppsett () {
	char filename[32] = "ARR_", extension[5] = ".DTA";
	char number[5] = "0000";
	int antSoner;

	oppsett = new Oppsett ();

	sprintf(number, "%04d", nummer);												// Setter sammen filnavn: "ARR_0001.DTA"
	strcat (filename, number);
	strcat (filename, extension);

	ifstream inn (filename);
	
	inn >> antSoner; inn.ignore ();													// Leser antall soner
																					// Laster inn gitt antall
	for (int i = 1; i <= antSoner; i++) {
		char *t = new char[STRLEN];
		int type;
		inn.ignore (); 
		inn.getline (t, STRLEN);													// Leser sone-navn
		inn >> type; inn.ignore ();
		if (type == 0) {
			Vrimle *vsone = new Vrimle (t, inn);									// Hvis det er vrimle-sone
			vsone->lesBilletter (inn);
			oppsett->soner->add (vsone);
		} else {
			Stoler *ssone = new Stoler (t, inn);									// Hvis det er stol-sone
			ssone->lesBilletter (inn);
			oppsett->soner->add (ssone);
		}	
	}

	inn.close ();
}

void Arrangement::lagre (ofstream &ut) {
	ut << '\n' << text << '\n';														// Skriver ut data
	ut << nummer << '\n';
	ut << spillested << '\n';
	ut << artist << '\n';
	ut << dato << '\n';
	ut << minutt << '\n';
	ut << time << '\n';
	ut << type << '\n';
}

void Arrangement::display () {
	cout << '\n' << nummer << ": " << text;											// Skriver alle private variabler
	cout << '\n' << spillested << ", " << dato;
	printf (", kl.%02d:%02d", time, minutt);
	cout << '\n' << artist << "\nType: " << type << '\n';
}

void Arrangement::displayAll () {
	Arrangement::display ();														// Skriver alle private variabler

	lastOppsett ();
																					// G�r igjennom alle soner i oppsett
	for (int i = 1; i <= oppsett->soner->no_of_elements (); i++) {
		Sone *sone = (Sone*)oppsett->soner->remove_no (i);							// Henter ut sone
		sone->display ();															// Skriver sone-data
		oppsett->soner->add (sone);													// Legger tilbake i listen
	}

	oppsett = NULL;																	// Frigj�r oppsett fra minne
}