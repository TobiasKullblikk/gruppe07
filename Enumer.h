#ifndef __ENUMER_H_INCLUDED__
#define __ENUMER_H_INCLUDED__		// Definerer Enumer.h slik at den ikke gjen-inkluderes

enum ArrangementType { Musikk, Sport, Teater, Show, Kino, Familie, Festival };

#endif